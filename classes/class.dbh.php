<?php
include_once('config.php');
include_once('classes/class.db.php');

class DBH extends DB
{
    public static function getAllowedUsers() {
        $response = array();
        $qry = new parent;
        $sql="SELECT * FROM allowed_users";
        $result = $qry->query($sql);
        while ($row = $result->fetch_assoc()) {
            $response[] = $row;
        }
        return $response;
    }

    public static function getExtractions() {
        $response = array();
        $qry = new parent;
        $sql="SELECT c.*, s.structureName, s.typeID, s.fuelExpires, s.state, s.corporationID, ms.solarSystemName, it.typeName, o.lastOres, o.nextOres, o.mined
              FROM `chunks` as c LEFT JOIN structures AS s ON c.structureID = s.structureID
              LEFT JOIN mapSolarSystems AS ms ON ms.solarSystemID = s.solarSystemID
              LEFT JOIN invTypes AS it ON it.typeID = s.typeID
              LEFT JOIN ores AS o ON o.structureID = s.structureID
              WHERE c.natural_decay > NOW() OR lastChunk > DATE_SUB(NOW(), INTERVAL 4 DAY) ORDER BY c.arrival";
        $result = $qry->query($sql);
        while ($row = $result->fetch_assoc()) {
            $response[] = $row;
        }
        $array1 = array();
        $array2 = array();
        foreach ($response as $r) {
            if ($r['lastChunk'] && (strtotime("now") - strtotime($r['lastChunk']) < 3600*24*4) ) {
                $array1[] = $r;
            } else {
                $array2[] = $r;
            }
        }
        if (count($array1) && count($array2)) {
            return array_merge($array1, $array2);
        } elseif (count($array1)) {
            return $array1;
        } else {
            return $array2;
        }
    }

    public static function getGooPrices() {
        $response = array();
        $qry = new parent;
        $sql="SELECT mp.*, it.typeName FROM mineralPrices AS mp LEFT JOIN invTypes AS it on it.typeID = mp.typeID";
        $result = $qry->query($sql);
        while ($row = $result->fetch_assoc()) {
            $response[] = $row;
        }
        return $response;
    }

    public static function getAllMineralIDs() {
        $response = array();
        $qry = new parent;
        $sql="SELECT it2.typeID FROM invMarketGroups as img 
        LEFT JOIN invMarketGroups AS img2 ON img2.parentGroupID = img.marketGroupID 
        LEFT JOIN invTypes AS it ON it.marketGroupID = img2.marketGroupID
        LEFT JOIN invTypeMaterials AS itm ON itm.typeID = it.typeID
        LEFT JOIN invTypes AS it2 ON it2.typeID = itm.materialTypeID
        WHERE img.parentGroupID = 1031
        GROUP BY it2.typeID";
        $result = $qry->query($sql);
        while ($row = $result->fetch_array()) {
            if ($row[0]) {
                 $response[] = $row[0];
            }
        }
        return $response;
    }

    public static function getMiners($structureID) {
        $response = array();
        $qry = new parent;
        $sql="SELECT * FROM ledgers WHERE structureID=".$structureID;
        $result = $qry->query($sql);
        while ($row = $result->fetch_assoc()) {
            $response[$row['characterID']] = array('corporationID' => $row['corporationID'], 
                                                   'mined' => json_decode($row['mined'], true), 
                                                   'start' => date('m/d', strtotime($row['startDate'])),
                                                   'end' => date('m/d', strtotime($row['endDate'])),
                                                  );
        }
        return $response;
    }

    public static function getAllowedUserIds() {
        $response = array();
        $qry = new parent;
        $sql="SELECT id FROM allowed_users";
        $result = $qry->query($sql);
        while ($row = $result->fetch_assoc()) {
            $response[] = $row['id'];
        }
        return $response;
    }


    public static function getConfig($key) {
        $response = null;
        $qry = new parent;
        $sql="SELECT * FROM config WHERE cfg_key='".$key."'";
        $result = $qry->query($sql);
        while ($row = $result->fetch_assoc()) {
            $response = $row['value'];
        }
        return $response;
    }

    public static function setConfig($key, $value) {
        $response = null;
        $qry = new parent;
        $sql="REPLACE INTO config (cfg_key, value) VALUES ('".$key."', '".$value."')";
        $result = $qry->query($sql);
    }

    public static function addPullChar($char, $corp) {
        $response = null;
        $qry = new parent;
        $sql="REPLACE INTO pullChars (characterID, corporationID) VALUES ({$char},{$corp})";
        $result = $qry->query($sql);
    }

    public static function getPullChars() {
        $response = null;
        $qry = new parent;
        $sql="SELECT * FROM pullChars";
        $result = $qry->query($sql);
        while ($row = $result->fetch_assoc()) {
            $response[] = $row;
        }
        return $response;
    }

}
?>
