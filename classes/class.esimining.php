<?php
require_once('config.php');

use Swagger\Client\Configuration;
use Swagger\Client\ApiException;

require_once('classes/esi/autoload.php');
require_once('classes/class.esisso.php');

class ESIMINING extends ESISSO
{

    private $corporationID = null;

    public function __construct($characterID) {
        $this->corporationID = EVEHELPERS::getCorpForChar($characterID);
        parent::__construct(null, $characterID);
    }

    private function getIndustryApi() {
        if ($this->hasExpired()) {
            $this->verify();
        }
        $esiapi = new ESIAPI();
        $esiapi->setAccessToken($this->accessToken);
        $assetapi = $esiapi->getApi('Industry');
        return $assetapi;
    }

    private function getUniverseApi() {
        if ($this->hasExpired()) {
            $this->verify();
        }
        $esiapi = new ESIAPI();
        $esiapi->setAccessToken($this->accessToken);
        $assetapi = $esiapi->getApi('Universe');
        return $assetapi;
    }

    private function getNotificationApi() {
        if ($this->hasExpired()) {
            $this->verify();
        }
        $esiapi = new ESIAPI();
        $esiapi->setAccessToken($this->accessToken);
        $notificationapi = $esiapi->getApi('Character');
        return $notificationapi;
    }

    private function getCorporationApi() {
        if ($this->hasExpired()) {
            $this->verify();
        }
        $esiapi = new ESIAPI();
        $esiapi->setAccessToken($this->accessToken);
        $corpapi = $esiapi->getApi('Corporation');
        return $corpapi;
    }

    public function getNotifications() {
        $notificationsapi = $this->getNotificationApi();
        $notifications = array();
        $i = 0;
        try {
            $notifications = $notificationsapi->getCharactersCharacterIdNotifications($this->characterID);
        } catch (Exception $e) {
            $this->error = true;
            $this->message = 'Could not retrieve Notifications: '.$e->getMessage().PHP_EOL;
            $this->log->exception($e);
            return null;
        }
        return $notifications;
    }

    public function getExtractions() {
        $api = $this->getIndustryApi();
        try {
            $result = $api->getCorporationCorporationIdMiningExtractions($this->corporationID);
        } catch (ApiException $e) {
            $this->error = true;
            $this->message = 'Could not fetch extractions: '.$e->getMessage();
            $this->log->exception($e);
            return array();
        }
        $universeapi = $this->getUniverseApi();
        $promise = array();
        foreach ($result as $r) {
            $promise[$r->getStructureId()] = $universeapi->getUniverseStructuresStructureIdAsync($r->getStructureId());
        }
        $responses = GuzzleHttp\Promise\settle($promise)->wait();
        $structures = array();
        $types = array();
        $systems = array();
        foreach ($responses as $id => $response) {
            if ($response['state'] == 'fulfilled') {
                $s = $response['value'];
                $structures[$id] = array('id' => $id, 'name' => $s->getName(), 'typeID' => $s->getTypeId(), 'systemID' => $s->getSolarSystemId());
                $types[] = $s->getTypeId();
                $systems[] = $s->getSolarSystemId();
            } elseif ($response['state'] == 'rejected') {
                $this->log->error($response['reason']);
            }
        }
        $typedict = EVEHELPERS::getInvNames($types);
        $systemdict = EVEHELPERS::getSystemNames($systems);
        foreach ($structures as $id => $structure) {
            $structures[$id]['typeName'] = $typedict[$structure['typeID']];
            $structures[$id]['systemName'] = $systemdict[$structure['systemID']];
            $this->structureToDb($structures[$id]);
        }
        foreach ($result as $r) {
            $e = array();
            $e['arrival'] = $r->getChunkArrivalTime()->format('Y/m/d H:i');
            $e['start'] = $r->getExtractionStartTime()->format('Y/m/d H:i');
            $e['duration'] = $r->getExtractionStartTime()->diff($r->getChunkArrivalTime())->format('%a days');
            $e['natural_decay'] = $r->getNaturalDecayTime()->format('Y/m/d H:i');
            $e['structure'] = $structures[$r->getStructureId()];
            $extractions[] = $e;
            $this->chunkToDb($e);
        }
        usort($extractions, function ($item1, $item2) {
             return $item1['arrival'] <=> $item2['arrival'];
        });
    }

    public function getOres() {
        $extractions = DBH::getExtractions();
        if(!count($extractions)) {
            return;
        }
        $nextOres = array();
        $lastOres = array();
        foreach($extractions as $e) {
            $nextOres[$e['structureID']] = null;
            $lastOres[$e['structureID']] = null;
        }
        $notifications = $this->getNotifications();
        foreach($notifications as $n) {
            $type = $n->getType();
            $time = $n->getTimestamp();
            if(strpos($type, "MoonminingExtraction") === 0) {
                if(strpos($type, "MoonminingExtractionFinished") === 0) {
                    $extype = 'finished';
                } elseif(strpos($type, "MoonminingExtractionStarted") === 0) {
                    $extype = 'started';
                } else {
                    continue;
                }
                $structureId = null;
                $ores = array();
                $text = preg_split("/[\s,]+/", str_replace('  ', ' ', $n->getText()));
                $i = 0;
                do {
                    if(strpos($text[$i], 'oreVolumeByType') !== false) {
                        $i ++;
                        do {
                            try {
                                $type = (int)rtrim($text[$i], ':');
                                if(!$type) {
                                    break;
                                }
                                $i ++;
                                $ores[$type] = (float)$text[$i];
                                $i ++;
                            } catch (Exception $e) {
                                break;
                            }
                        } while ($i <= count($text));
                    }
                    if(strpos($text[$i], 'structureID') !== false) {
                        $i++;
                        $structureId = (int)$text[$i];
                    }
                    $i++;
                } while ($i < count($text));
                if($structureId && count($ores)) {
                    foreach($extractions as $i => $e) {
                        if($structureId == $e['structureID']) {
                            if(!$e['lastChunk'] && $extype == 'finished') {
                                $extractions[$i]['lastChunk'] = $time->format('Y-m-d H:i:s');
                                $e['lastChunk'] = $extractions[$i]['lastChunk'];
                                $qry = DB::getConnection();
                                $sql = "UPDATE chunks SET lastChunk = '".$extractions[$i]['lastChunk']."' WHERE structureID = ".$structureId;
                                $qry->query($sql);
                            }
                            if(abs(strtotime($e['lastChunk']) - $time->getTimestamp()) <= 3600*6 && $extype == 'finished' && !$lastOres[$structureId]) {
                                $lastOres[$structureId] = $ores;
                            } elseif (abs(strtotime($e['start']) - $time->getTimestamp()) <= 3600 && $extype == 'started' && !$nextOres[$structureId]) {
                                $nextOres[$structureId] = $ores;
                            }
                        }
                    }
                }
            }
        }

        foreach(array_keys($nextOres) as $id) {
            $qry = DB::getConnection();
            if ($lastOres[$id] && $nextOres[$id]) {
                $sql = "INSERT INTO ores (structureID, lastOres, nextOres) VALUES (".$id." ,'".json_encode($lastOres[$id])."','".json_encode($nextOres[$id])."') ON DUPLICATE KEY UPDATE lastOres='".json_encode($lastOres[$id])."', nextOres='".json_encode($nextOres[$id])."'";
                $qry->query($sql);
            } elseif ($lastOres[$id]) {
                $sql = "INSERT INTO ores (structureID, lastOres) VALUES (".$id." ,'".json_encode($lastOres[$id])."') ON DUPLICATE KEY UPDATE lastOres='".json_encode($lastOres[$id])."'";
                $qry->query($sql);
            } elseif ($nextOres[$id]) {
                $sql = "INSERT INTO ores (structureID, nextOres) VALUES (".$id." ,'".json_encode($nextOres[$id])."') ON DUPLICATE KEY UPDATE nextOres='".json_encode($nextOres[$id])."'";
                $qry->query($sql);
            }
        }
    }

    public function updateStructures() {
        $qry = DB::getConnection();
        $sql="SELECT structureID FROM structures";
        $result = $qry->query($sql);
        $structures = array();
        if($result->num_rows) {
            while ($row = $result->fetch_array()) {
                $structures[$row[0]] = array();
            }
        } else {
            return;
        }
        $api = $this->getCorporationApi();
        try {
            $fetch = $api->getCorporationsCorporationIdStructuresWithHttpInfo($this->corporationID);
        } catch (Exception $e) {
            $this->error = true;
            $this->message = 'Could not retrieve Corporation structures: '.$e->getMessage().PHP_EOL;
            $this->log->exception($e);
            return null;
        }
        $result = $fetch[0];
        $pages = ($fetch[2]['X-Pages'][0]);
        if ($pages > 1) {
            $i = 2;
            $promises = array();
            do {
                $promises[] = $api->getCorporationsCorporationIdStructuresAsync($this->corporationID, 'en-us', 'tranquility', null, 'en-us', (string)$i);
                $i += 1;
            } while ($i <= $pages);
            $responses = GuzzleHttp\Promise\settle($promises)->wait();
            foreach ($responses as $response) {
                if($response['state'] == 'fulfilled') {
                    $result = array_merge($result, ($response['value']));
                } elseif ($response['state'] == 'rejected') {
                    $this->log->error($response['reason']);
                }
            }
        }
        if (!count($result)) {
            return;
        }
        foreach ($result as $r) {
            $id = $r->getStructureId();
            if (isset($structures[$id])) {
                $structures[$id]['fuelExpires'] = $r->getFuelExpires()->format('Y-m-d H:i:s');
                $structures[$id]['state'] = $r->getState();
            }
        }
        $stmt = $qry->prepare("UPDATE structures SET fuelExpires=?, state=?, lastUpdate=NOW() WHERE structureID=?");
        if ($stmt) {
            $stmt->bind_param('ssi', $fuel, $state, $sid);
            foreach ($structures as $id => $s) {
                $fuel = $s['fuelExpires'];
                $state = $s['state'];
                $sid = $id;
                $stmt->execute();
            }
            $stmt->close();
        }
    }

    public function getLedgers() {
        $extractions = DBH::getExtractions();
        if(!count($extractions)) {
            return;
        }
        $mined_units = array();
        $mined = array();
        $ledgers = array();
        foreach($extractions as $e) {
            if(!$e['lastChunk']) {
                continue;
            }
            if($e['corporationID'] != $this->corporationID) {
                continue;
            }
            $last = strtotime($e['lastChunk']);
            if(time() - $last > (3600*24*29)) {
                continue;
            }
            $structureId = $e['structureID'];
            $mined_units[$structureId] = array();
            $mined[$structureId] = array();
            $api = $this->getIndustryApi();
            $ele = array();
            try {
                $fetch = $api->getCorporationCorporationIdMiningObserversObserverIdWithHttpInfo($this->corporationID, $structureId);
            } catch (Exception $e) {
                $this->error = true;
                $this->message = 'Could not retrieve Ledger: '.$e->getMessage().PHP_EOL;
                $this->log->exception($e);
                return null;
            }
            $result = $fetch[0];
            $pages = ($fetch[2]['X-Pages'][0]);
            if ($pages > 1) {
                $i = 2;
                $promises = array();
                do {
                    $promises[] = $api->getCorporationCorporationIdMiningObserversObserverIdAsync($this->corporationID, $structureId, 'tranquility', null, (string)$i);
                    $i += 1;
                } while ($i <= $pages);
                $responses = GuzzleHttp\Promise\settle($promises)->wait();
                foreach ($responses as $response) {
                    if($response['state'] == 'fulfilled') {
                        $result = array_merge($result, ($response['value']));
                    } elseif ($response['state'] == 'rejected') {
                        $this->log->error($response['reason']);
                    } 
                }
            }
            if (!count($result)) {
                continue;
            }
            $ledgers[$structureId] = array();
            foreach ($result as $r) {
                $stamp = $r->getLastUpdated()->getTimestamp();
                if ($stamp > $last - (3600*24) && $stamp < $last + (3600*24*4)) {
                    if (!isset($mined_units[$structureId][$r->getTypeId()])) {
                        $mined_units[$structureId][$r->getTypeId()] = $r->getQuantity();
                    } else {
                        $mined_units[$structureId][$r->getTypeId()] += $r->getQuantity();
                    }
                    $charID = $r->getCharacterId();
                    if (!isset($ledgers[$structureId][$charID])) {
                        $ledgers[$structureId][$charID] = array('start' => $stamp, 'end' => $stamp, 'mined' => array(), 'corporationID' => $r->getRecordedCorporationId());
                    }
                    if ($stamp < $ledgers[$structureId][$charID]['start']) {
                         $ledgers[$structureId][$charID]['start'] = $stamp;
                    } elseif ($stamp > $ledgers[$structureId][$charID]['end']) {
                         $ledgers[$structureId][$charID]['end'] = $stamp;
                    }
                    if (!isset($ledgers[$structureId][$charID]['mined'][$r->getTypeId()])) {
                        $ledgers[$structureId][$charID]['mined'][$r->getTypeId()] = $r->getQuantity();
                    } else {
                        $ledgers[$structureId][$charID]['mined'][$r->getTypeId()] += $r->getQuantity();
                    }
                }
            }
            $qry = DB::getConnection();
            foreach($mined_units as $s => $ores) {
                $oreinfo = EVEHELPERS::getOreInfo(array_keys($ores));
                foreach($ores as $id => $qty) {
                    $mined[$s][$id] = $qty * $oreinfo[$id]['volume'];
                }
                if (count((array)$mined[$s])) {
                    $sql="UPDATE ores SET mined = '".json_encode($mined[$s])."' WHERE structureID=".$s;
                    $qry->query($sql);
                }
            }
            if (count($ledgers)) {
                $stmt = $qry->prepare("REPLACE INTO ledgers (structureID, characterID, corporationID, mined, startDate, endDate) VALUE (?, ?, ?, ?, ?, ?)");
                if ($stmt) {
                    $stmt->bind_param('iiisss', $_structureid, $_charid, $_corpid, $_mined, $_start, $_end);
                    foreach ($ledgers as $struc => $l) {
                        foreach ($l as $c => $m) {
                            $_structureid = $struc;
                            $_charid = $c;
                            $_corpid = $m['corporationID'];
                            $_mined = json_encode($m['mined']);
                            $_start = date('Y-m-d H:i:s', $m['start']);
                            $_end = date('Y-m-d H:i:s', $m['end']);
                            $stmt->execute();
                        }
                    }
                    $stmt->close();
                }
            }
        }
    }

    public static function updateMineralPrices() {
        $minerals = DBH::getAllMineralIDs();
        $esiapi = new ESIAPI();
        $marketapi = $esiapi->getApi('Market');
        #Jita:
        $station_id = 60003760;
        $system_id = 30000142;
        $region_id = 10000002;
        foreach($minerals as $i) {
            $promises[] = $marketapi->getMarketsRegionIdOrdersAsync('sell', $region_id, "tranquility", null, 1, $i);
        }
        $responses = GuzzleHttp\Promise\settle($promises)->wait();
        $prices = array_fill_keys($minerals, 0);
        foreach($responses as $response) {
            if ($response['state'] == 'fulfilled') {
                if (!isset($response['value'][0]) || $response['value'][0] == null) {
                    continue;
                }
                $typeid = $response['value'][0]->getTypeId();
                $orders = [];
                $totalvol = 0;
                foreach($response['value'] as $r) {
                    if ($r->getLocationId() == $station_id) {
                        if (isset($orders[$r->getPrice()*100])) {
                            $orders[$r->getPrice()*100] += $r->getVolumeRemain();
                        } else {
                            $orders[$r->getPrice()*100] = $r->getVolumeRemain();
                        }
                    }
                }
                ksort($orders);
                if (count($orders) > 5) {
                    $orders = array_slice($orders, 0, (int)floor(count($orders)*0.98), true);
                }
                $totalvol = array_sum($orders);
                $running = 0;
                $prev_perc = 0;
                $prev_price = 0;
                foreach($orders as $p => $q) {
                    $running += $q;
                    $perc = ($running/$totalvol) * 100;
                    if ($perc >= 5 || ($perc > 5 && $prev_perc == 0) ) {
                        $prices[$typeid] = (int)round($p, 0);
                        break;
                    } elseif ($perc > 5) {
                        $ratio = ( $perc/($perc-$prev_perc) );
                        $prices[$typeid] = (int)round($p*$ratio + $prev_price*(1-$ratio), 0);
                        break;
                    }
                    $prev_perc = $perc;
                    $prev_price = $p;
                }
            }
        }
        $qry = DB::getConnection();
        $stmt = $qry->prepare("REPLACE INTO mineralPrices (typeID, price, lastUpdate) VALUES (?, ?, NOW())");
        if ($stmt) {
            $stmt->bind_param('ii', $_id, $_price);
            foreach ($prices as $id => $price) {
                if ($price != 0) {
                    $_id = $id;
                    $_price = $price;
                    $stmt->execute();
                }
            }
            $stmt->close();
        }
    }

    public static function getRefinedValue($id, $qty = 1, $yield = 0.75) {
        $qry = DB::getConnection();
        $value = (float)0;
        $sql="SELECT it.typeID, it.portionSize, itm.quantity, mp.price FROM invTypes as it 
        LEFT JOIN invTypeMaterials AS itm ON it.typeID = itm.typeID 
        LEFT JOIN mineralPrices AS mp ON itm.materialTypeID = mp.typeID
        WHERE it.typeID = ".$id;
        $result = $qry->query($sql);
        $return = array();
        if($result->num_rows) {
            while ($row = $result->fetch_assoc()) {
                $value += ($qty/$row['portionSize'])*$yield*$row['quantity']*($row['price']/100);
            }
        }
        return $value;
    }

    private function structureToDb($s) {
        $qry = DB::getConnection();
        $stmt = $qry->prepare("INSERT INTO structures (solarSystemID, structureID, corporationID, structureName, typeID, lastUpdate) VALUES (?, ?, ?, ?, ?, NOW()) ON DUPLICATE KEY UPDATE structureName=?, lastUpdate = NOW()");
        if ($stmt) {
            $stmt->bind_param('iiisis', $system, $structure, $corp, $name, $type, $name2);
            $system = $s['systemID'];
            $structure = $s['id'];
            $name = $s['name'];
            $corp = $this->corporationID;
            $type = $s['typeID'];
            $name2 = $s['name'];
            $stmt->execute();
            if ($stmt->errno) {
                $this->error = true;
                $this->message = $stmt->error;
                $this->log->error($this->message);
            }
            $stmt->close();
        }
    }

    private function chunkToDb($extraction) {
        $structureID = $extraction['structure']['id'];
        $start = date('Y-m-d H:i:s', strtotime($extraction['start']));
        $arrival = strtotime($extraction['arrival']);
        $decay = date('Y-m-d H:i:s', strtotime($extraction['natural_decay']));
        $qry = DB::getConnection();
        $sql="SELECT * FROM chunks WHERE structureID=".$structureID;
        $result = $qry->query($sql);
        $return = array();
        if($result->num_rows) {
            while ($row = $result->fetch_assoc()) {
                $db_lastChunk = strtotime($row['lastChunk']);
                $db_nextChunk = strtotime($row['natural_decay']);
            }
        } else {
            $db_lastChunk = null;
            $db_nextChunk = null;
        }
        $next = date('Y-m-d H:i:s', $arrival);
        if(!$db_nextChunk) {
            $sql="REPLACE INTO chunks (structureID, lastChunk, start, arrival, natural_decay) VALUES ({$structureID},NULL,'{$start}','{$next}','{$decay}')";
        } else {
            if(!$db_lastChunk) {
                $last = null;
            } else {
                $last = date('Y-m-d H:i:s', $db_lastChunk);
            }
            if($db_nextChunk < $arrival) {
                $last = date('Y-m-d H:i:s', $db_nextChunk);
                $sql = "UPDATE ores SET lastOres=nextOres, nextOres=NULL, mined=NULL WHERE structureID={$structureID}";
                $qry->query($sql);
            }
            if($last) {
                $sql="REPLACE INTO chunks (structureID, lastChunk, start, arrival, natural_decay) VALUES ({$structureID},'{$last}','{$start}','{$next}','{$decay}')";
            } else {
                $sql="REPLACE INTO chunks (structureID, lastChunk, start, arrival, natural_decay) VALUES ({$structureID},NULL,'{$start}','{$next}','{$decay}')";
            }
        }
        $qry->query($sql);
    }

    public static function checkExpired() {
        $qry = DB::getConnection();
        $sql="SELECT * FROM chunks WHERE natural_decay < NOW()";
        $result = $qry->query($sql);
        $todo = [];
        if($result->num_rows) {
            while ($row = $result->fetch_assoc()) {
                $todo[] = $row['structureID'];
            }
        }
        foreach ($todo as $structureID) {
            $sql = "UPDATE ores SET lastOres=nextOres, nextOres=NULL, mined=NULL WHERE structureID={$structureID}";
            $qry->query($sql);
            $sql = "UPDATE chunks SET lastChunk=natural_decay, start=NULL, arrival=NULL, natural_decay=NULL WHERE structureID={$structureID}";
            $qry->query($sql);
        }
        $sql = "DELETE ledgers FROM ledgers LEFT JOIN chunks ON chunks.structureID = ledgers.structureID WHERE ledgers.endDate < DATE_SUB(chunks.lastChunk, INTERVAL 2 DAY)";
        $qry->query($sql);
    }

}
?>
