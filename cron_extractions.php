<?php
chdir(dirname(__FILE__));

require_once('config.php');
require_once('loadclasses.php');

$allow_remote_usage = False;

if (php_sapi_name() !='cli' && !$allow_remote_usage) {
    $page = new Page('Access denied');
    $page->setError('This Cron job may only be run from the command line.');
    $page->display();
    exit;
}

if (!ESIAPI::checkTQ()) {
    exit;
}

$log = new ESILOG('log/cron_fleets.log');

class Cronlock {
    private static $lockhandle = null;
    private static $lockfile = __DIR__.'/cron_extractions.lock';

    public static function getLock() {
        self::$lockhandle = fopen(self::$lockfile, 'w');
        if (!flock(self::$lockhandle, LOCK_EX | LOCK_NB)) {
            $log = new ESILOG('log/cron_extractions.log');
            $log->error('Could not get lock. Maybe another instance is still running.');
            fclose(self::$lockhandle);
            exit;
        }
    }

    public static function unLock() {
        if (is_resource(self::$lockhandle)) {
            flock(self::$lockhandle, LOCK_UN);
            fclose(self::$lockhandle);
        }
        if(file_exists(self::$lockfile)) {
            unlink(self::$lockfile);
        }
        exit;
    }
}

Cronlock::getLock();

function abort_handler() {
    Cronlock::unLock();
}
register_shutdown_function("abort_handler");

ESIMINING::updateMineralPrices();
if ($chars = DBH::getPullChars()) {
    foreach($chars as $c) {
        $mining = new ESIMINING($c['characterID']);
        $mining->getExtractions();
        $mining->updateStructures();
        $mining->getOres();
        $mining->getLedgers();
    }
    ESIMINING::checkExpired();
} else {
    Cronlock::unLock();
}

Cronlock::unLock();
?>
