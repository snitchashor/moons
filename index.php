<?php

$start_time = microtime(true);

require_once('loadclasses.php');
require_once('auth.php');

if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin']) {
  $admin = true;
} else {
  $admin = false;
}


$page = new Page('BLOC moon mining thingy');

$extractions = DBH::getExtractions();

if (!count($extractions)) {
    $page->addBody("Nothing to see here.");
    $page->display();
    exit;
}

function formatIsk($value) {
    if ($value > 1000000000) {
        return round($value/1000000000, 2).'b ISK';
    } elseif ($value > 1000000) {
        return round($value/1000000, 1).'m ISK';
    } elseif ($value > 1000) {
        return round($value/1000, 0).'k ISK';
    } else {
         return round($value, 0).' ISK';
    }
}

function timeDiff($time) {
    if ($time instanceof DateTime) {
        $time = $time->getTimestamp();
    } elseif (!is_numeric($time)) {
        $time = strtotime($time);
    }
    $delta = $time - time();
    if ($delta < 0) {
        $delta = abs($delta);
        if($delta < 3570) {
            $t = (int)round($delta/60, 0);
            return '&nbsp;<span class="btn-xs bg-success">'.(string)$t.' minute'.($t>1?'s':'').' ago</span>';
        } elseif($delta < 3600*23.5) {
            $t = (int)round($delta/3600, 0);
            return '&nbsp;<span class="btn-xs bg-success">'.(string)$t.' hour'.($t>1?'s':'').' ago</span>';
        } else {
            $t = (int)round($delta/(3600*24), 0);
            if($t < 2) {
                $btnclass = 'bg-warning';
            } elseif ($t < 4) {
                $btnclass = 'bg-danger';
            } else {
                $btnclass = 'btn-default disabled';
            }
            return '&nbsp;<span class="btn-xs '.$btnclass.'">'.(string)$t.' day'.($t>1?'s':'').' ago</span>';
        }
    } else {
        if($delta < 3570) {
            $t = (int)round($delta/60, 0);
            return '&nbsp;<span class="btn-xs bg-success">in '.(string)$t.' minute'.($t>1?'s':'').'</span>';
        } elseif($delta < 3600*23.5) {
            $t = (int)round($delta/3600, 0);
            return '&nbsp;<span class="btn-xs bg-primary">in '.(string)$t.' hour'.($t>1?'s':'').'</span>';
        } else {
            $t = (int)round($delta/(3600*24), 0);
            return '&nbsp;<span class="btn-xs '.($t>2?'btn-default disabled':'bg-primary').'">in '.(string)$t.' day'.($t>1?'s':'').'</span>';
        }
    }
}

function fuelTime($time) {
    if ($time instanceof DateTime) {
        $time = $time->getTimestamp();
    } elseif (!is_numeric($time)) {
        $time = strtotime($time);
    }
    $delta = $time - time();
    if($delta < 3600*23.5) {
        $t = (int)round($delta/60, 0);
        return '&nbsp;<span class="btn-xs bg-danger"><i class="fas fa-gas-pump"></i>&nbsp;'.(string)$t.' minute'.($t>1?'s':'').'</span>';
    } elseif($delta < 3600*23.5) {
        $t = (int)round($delta/3600, 0);
        return '&nbsp;<span class="btn-xs bg-danger"><i class="fas fa-gas-pump"></i>&nbsp;'.(string)$t.' hour'.($t>1?'s':'').'</span>';
    } else {
        $t = (int)round($delta/(3600*24), 0);
        return '&nbsp;<span class="btn-xs '.($t>2?'bg-success':'bg-warning').'"><i class="fas fa-gas-pump"></i>&nbsp;'.(string)$t.' day'.($t>1?'s':'').'</span>';
    }

}

$html = '<h5>Upcoming extractions</h5><div class="row">';

foreach ($extractions as $e) {
    $html .= '<div class="col-lg-6 col-md-12"><div class="well well-sm">';
    $html .= '<h6><img class="img-rounded" src="https://image.eveonline.com/Type/'.$e['typeID'].'_32.png">&nbsp;'.$e['typeName'].' \''.$e['structureName'].'\' in '.$e['solarSystemName'].fuelTime($e['fuelExpires']).'</h6>';
    if ($e['arrival']) {
        $html .= '<table class="table small table-condensed"><tr><td>Chunk arrival:</td><td>'.date('Y/m/d H:i', strtotime($e['arrival'])).' '.timeDiff($e['arrival']).'</td></tr>';
        $html .= '<tr><td>Natural decay:</td><td>'.date('Y/m/d H:i', strtotime($e['natural_decay'])).'</td></tr>';
        $html .= '<tr><td>Extraction duration:</td><td>'.(int)round((strtotime($e['arrival'])-strtotime($e['start']))/(3600*24), 0).' days</td></tr></table>';
        if ($e['nextOres']) {
            $total = 0;
            $ores = json_decode($e['nextOres'], true);
            $oreinfo = EVEHELPERS::getOreInfo(array_keys($ores));
            $html .= '<h6>Estimated Ores*</h6><table class="table small table-condensed">';
            foreach ($ores as $id => $qty) {
                $refVal = ESIMINING::getRefinedValue($id, $qty)/$oreinfo[$id]['volume'];
                $html .= '<tr><td><img class="img-rounded" height="24px" src="https://image.eveonline.com/Type/'.$id.'_32.png">&nbsp;'.$oreinfo[$id]['name'].'</td><td class="text-right">'.number_format($qty/1000).'k m&sup3;</td><td class="text-right">'.formatIsk($refVal).' ('.round($refVal/$qty, 1).' ISK/m&sup3;)</td></tr>';
                $total += $refVal;
            }
            $html .= '</table><h6>Total:  '. formatIsk($total).'</h6>';
        }
        
    }
    if ($e['lastChunk']) {
        $html .= '<span>Last Chunk cracked '.timeDiff($e['lastChunk']).'</span>';
        if(time() - strtotime($e['lastChunk']) <= 3600*24*26 && $e['lastOres'] && $admin) {
            $html .= '&nbsp;<a href="'.URL::url_path().'ledgers.php?structureID='.$e['structureID'].'" target="_blank" class="btn btn-xs btn-primary">Show miners</a>';
        }
        if(time() - strtotime($e['lastChunk']) <= 3600*24*4 && $e['lastOres']) {
            if (!$e['mined']) {
                $e['mined'] = '[]';
            }
            if(time() - strtotime($e['lastChunk']) <= 3600*24*2) {
                $divclass = ' in';
            } else {
                $divclass = '';
            }
            $total = 0;
            $html .= '&nbsp;<button class="btn btn-xs btn-primary" type="button" data-toggle="collapse" data-target="#mined-'.$e['structureID'].'" aria-expanded="false" aria-controls="">Toggle info</button>
                      <div class="collapse'.$divclass.'" id="mined-'.$e['structureID'].'">
                          <h6>Minerals left (estimate):</h6><table class="table small table-condensed">';
            $ores = json_decode($e['lastOres'], true);
            $mined = json_decode($e['mined'], true);
            $oreinfo = EVEHELPERS::getOreInfo(array_merge(array_keys($ores), array_keys($mined)));
            foreach ($mined as $id => $qty) {
                if(!in_array($id, array_keys($ores))) {
                    $name = $oreinfo[$id]['name'];
                    foreach (array_keys($ores) as $o) {
                        if(strrpos($name, $oreinfo[$o]['name'], 0) === (strlen($name) - strlen($oreinfo[$o]['name'])) ) {
                            if(isset($mined[$o])) {
                                $mined[$o] += $qty;
                            } else {
                                $mined[$o] = $qty;
                            }
                        }
                    }
                }
            }
            foreach ($ores as $id => $qty) {
                if (isset($mined[$id])) {
                    $left = $qty - $mined[$id];
                } else {
                    $left = $qty;
                }
                if ($left < 1000) {
                    $left = 0;
                }
                if ($left > 0) {
                    $refVal = ESIMINING::getRefinedValue($id, $left)/$oreinfo[$id]['volume'];
                    $isk_m3 = round($refVal/$left, 1);
                } else {
                    $refVal = 0;
                    $isk_m3 = round(ESIMINING::getRefinedValue($id)/$oreinfo[$id]['volume'], 1);
                }
                $perc = (int)round($left*100/$qty, 0);
                $html .= '<tr><td><img class="img-rounded" height="24px" src="https://image.eveonline.com/Type/'.$id.'_32.png">&nbsp;'.$oreinfo[$id]['name'].'</td><td class="text-right">'.number_format($left/1000).'k m&sup3; / '.number_format($qty/1000).'k m&sup3; </td><td class="text-right">'.formatIsk($refVal).' ('.$isk_m3.' ISK/m&sup3;)</td></tr>';
                $html .= '<tr><td colspan="3"><div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="'.$perc.'"aria-valuemin="0" aria-valuemax="100" style="width:'.$perc.'%">'.$perc.'%</div>
                          </div></td></tr>';
                $total += $refVal;
            }
            $html .= '</table><h6>Total:  '. formatIsk($total).'</h6></div>';
        }
        
    }
    $html .= '</div></div>';

}
$html .='<div class="col-lg-12"><span>*All ISK values are calcuated using the 5th percentile of Jita sell orders, assuming 75% refinement yield.</span></div></div>';

$page->addBody($html);

$page->setBuildTime(number_format(microtime(true) - $start_time, 3));
$page->display();
exit;
?>
