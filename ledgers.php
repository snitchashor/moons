<?php
$start_time = microtime(true);
require_once('auth.php');
require_once('config.php');
require_once('loadclasses.php');


if (!isset($_SESSION['isAdmin']) || !$_SESSION['isAdmin']) {
    $page = new Page('Access denied');
    $page->setError('You\'re not allowed to do this.');
    $page->display();
    exit;
}

if (isset($_GET['structureID']) && $_GET['structureID']) {
    $structureID = $_GET['structureID'];
} else {
    $page = new Page('Missing information');
    $page->setError('No structure ID specified.');
    $page->display();
    exit;
}

function formatIsk($value) {
    if ($value > 1000000000) {
        return round($value/1000000000, 2).'b';
    } elseif ($value > 1000000) {
        return round($value/1000000, 1).'m';
    } elseif ($value > 1000) {
        return round($value/1000, 0).'k';
    } else {
         return round($value, 0);
    }
}

$miners = (DBH::getMiners($structureID));
$dict = EVEHELPERS::esiIdsToNames(array_merge(array_keys($miners), array_unique(array_column($miners, 'corporationID')) ));

$temp = array();
foreach (array_column($miners, 'mined') as $mined) {
    foreach($mined as $o => $qty) {
        $temp[$o] = 1;
    }
}
$oreinfo = EVEHELPERS::getOreInfo(array_keys($temp));

$html = '<table class="table table-condensed table-striped small datatable">
             <thead>
                 <th>From</th>
                 <th>To</th>
                 <th>Character</th>
                 <th>Corporation</th>';
foreach ($oreinfo as $id => $o) {
    $html .= '<th class="num"><img class="img" height="20px" src="https://image.eveonline.com/Type/'.$id.'_32.png">&nbsp;'.$o['name'].'</th>';
}
$html .= '<th class="num">Est. value</th></thead><tbody>';
foreach ($miners as $charid => $m) {
    $total = 0;
    $html .= '<tr><td>'.$m['start'].'</td><td>'.$m['end'].'</td>
                  <td data-sort="'.$dict[$charid].'"><img class="img-rounded" height="24px" src="https://imageserver.eveonline.com/Character/'.$charid.'_32.jpg">&nbsp;'.$dict[$charid].'</td>
                  <td data-sort="'.$dict[$m['corporationID']].'"><img class="img-rounded" height="24px" src="https://imageserver.eveonline.com/Corporation/'.$m['corporationID'].'_32.png">&nbsp;'.$dict[$m['corporationID']].'</td>';
    foreach ($oreinfo as $id => $o) {
        if (isset($m['mined'][$id])) {
            $html .= '<td data-sort="'.$m['mined'][$id].'">'.number_format($m['mined'][$id]).'</td>';
            $total += ESIMINING::getRefinedValue($id, $m['mined'][$id]);
        } else {
            $html .= '<td></td>';
        }
    }
    $html .= '<td data-sort="'.$total.'">'.formatIsk($total).'</td></tr>';
}

$footer = '<script>$(document).ready(function() {
            var table = $(".datatable").dataTable(
               {
                   "bPaginate": true,
                   "pageLength": 25,
                   "aoColumnDefs" : [ {
                       "bSortable" : false,
                       "aTargets" : [ "no-sort" ]
                   }, {
                       "sClass" : "num-col",
                       "aTargets" : [ "num" ],
                   } ],
                   fixedHeader: {
                       header: true,
                       footer: false
                   },
                   "order": [ 2, "asc" ],
               });
           });
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/dt-custom.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.min.css" integrity="sha256-C4J6NW3obn7eEgdECI2D1pMBTve41JFWQs0UTboJSTg=" crossorigin="anonymous" />';

$html .= '</tbody></table>';
$page = new Page('Mining Ledgers for '.EVEHELPERS::getStructureNames([$structureID])[$structureID]);
$page->addBody($html);
$page->addFooter($footer);
$page->setBuildTime(number_format(microtime(true) - $start_time, 3));
$page->display();
exit;
?>
