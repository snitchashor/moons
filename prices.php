<?php
require_once('config.php');
require_once('loadclasses.php');
require_once('auth.php');

$prices = DBH::getGooPrices();
$html = '<p>Moon goo prices used in ore value calculations.</p>';
$html .= '<div class="row"><div class="col-xs-12 col-sm-10 col-md-7 col-lg-5"><table class="table table-striped table-condensed small">
             <thead><th>Moon goo</th><th class="num-col">Price</th></thead>
             <tbody>';
foreach($prices as $p) {
    $html .= '<tr><td><img class="img-rounded" height="24px" src="https://image.eveonline.com/Type/'.$p['typeID'].'_32.png">&nbsp;'.$p['typeName'].'</td><td class="num-col">'.number_format($p['price']/100, 2).' ISK</td></tr>';
}
$html .= '</tbody></table></div></div>';

$page = new Page('Price table');
$page->addBody($html);
$page->display();

?>
