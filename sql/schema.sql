SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `allowed_users` (
  `id` bigint(16) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `authTokens` (
  `id` int(11) NOT NULL,
  `selector` char(32) DEFAULT NULL,
  `token` char(64) DEFAULT NULL,
  `characterID` bigint(16) NOT NULL,
  `expires` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chunks` (
  `structureID` bigint(20) NOT NULL,
  `lastChunk` datetime DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `arrival` datetime DEFAULT NULL,
  `natural_decay` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `config` (
  `cfg_key` varchar(32) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `esisso` (
  `id` int(11) NOT NULL,
  `characterID` bigint(16) NOT NULL,
  `characterName` varchar(255) DEFAULT NULL,
  `refreshToken` varchar(500) NOT NULL,
  `accessToken` varchar(4096) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ownerHash` varchar(255) NOT NULL,
  `failcount` int(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ledgers` (
  `characterID` bigint(20) NOT NULL,
  `corporationID` bigint(20) NOT NULL,
  `structureID` bigint(20) NOT NULL,
  `mined` json DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mineralPrices` (
  `typeID` bigint(20) NOT NULL,
  `price` bigint(20) NOT NULL,
  `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ores` (
  `structureID` bigint(20) NOT NULL,
  `lastOres` json DEFAULT NULL,
  `nextOres` json DEFAULT NULL,
  `mined` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `pullChars` (
  `characterID` bigint(20) NOT NULL,
  `corporationID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `structures` (
  `solarSystemID` int(11) NOT NULL,
  `structureID` bigint(16) NOT NULL,
  `corporationID` bigint(20) DEFAULT NULL,
  `structureName` varchar(255) DEFAULT NULL,
  `typeID` int(11) NOT NULL,
  `fuelExpires` datetime DEFAULT NULL,
  `state` varchar(128) DEFAULT NULL,
  `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `allowed_users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `authTokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `characterID` (`characterID`);

ALTER TABLE `chunks`
  ADD PRIMARY KEY (`structureID`),
  ADD UNIQUE KEY `structureID` (`structureID`);

ALTER TABLE `config`
  ADD PRIMARY KEY (`cfg_key`),
  ADD UNIQUE KEY `cfg_key` (`cfg_key`);

ALTER TABLE `esisso`
  ADD PRIMARY KEY (`characterID`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `characterID` (`characterID`);

ALTER TABLE `ledgers`
  ADD UNIQUE KEY `characterID` (`characterID`,`structureID`) USING BTREE;

ALTER TABLE `mineralPrices`
  ADD PRIMARY KEY (`typeID`),
  ADD UNIQUE KEY `typeID` (`typeID`);

ALTER TABLE `ores`
  ADD PRIMARY KEY (`structureID`),
  ADD UNIQUE KEY `structureID` (`structureID`);

ALTER TABLE `pullChars`
  ADD PRIMARY KEY (`characterID`),
  ADD UNIQUE KEY `characterID` (`characterID`);

ALTER TABLE `structures`
  ADD PRIMARY KEY (`structureID`),
  ADD UNIQUE KEY `structureID` (`structureID`),
  ADD KEY `structureID_2` (`structureID`);


ALTER TABLE `authTokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `esisso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
